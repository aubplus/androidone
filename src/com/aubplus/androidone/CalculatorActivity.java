package com.aubplus.androidone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * This is another Activity, called CalculatorActivity. We will build a custom calculator that takes
 * two EditTexts (text fields) and three Buttons (+, -, *). Clicking on each Button computes the
 * respective operation and sends it back to the WelcomeActivity.
 * 
 * @author karim
 */
public class CalculatorActivity extends Activity {

	private EditText aEditText;
	private EditText bEditText;
	private Button sumButton;
	private Button diffButton;
	private Button productButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);

		aEditText = (EditText) findViewById(R.id.aEditText);
		bEditText = (EditText) findViewById(R.id.bEditText);
		sumButton = (Button) findViewById(R.id.sumButton);
		diffButton = (Button) findViewById(R.id.diffButton);
		productButton = (Button) findViewById(R.id.productButton);

		sumButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int a = Integer.parseInt(aEditText.getText().toString());
				int b = Integer.parseInt(bEditText.getText().toString());

				int sum = a + b;

				// Creates a new Intent (what is an Intent already?)
				Intent i = new Intent();

				// Puts a (key, value) pair in its Extras. In our case, we want to pass the 'sum'
				// result back to the previous Activity.
				i.putExtra(Globals.ourOperationResult, sum);

				// Call this to set the result that your activity will return to its caller.
				setResult(RESULT_OK, i);

				// Call 'finish()' to close and end the Activity.
				finish();
			}
		});

		diffButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int a = Integer.parseInt(aEditText.getText().toString());
				int b = Integer.parseInt(bEditText.getText().toString());

				int diff = a - b;

				Intent i = new Intent();
				i.putExtra(Globals.ourOperationResult, diff);

				setResult(RESULT_OK, i);
				finish();
			}
		});

		productButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int a = Integer.parseInt(aEditText.getText().toString());
				int b = Integer.parseInt(bEditText.getText().toString());

				int product = a * b;

				Intent i = new Intent();
				i.putExtra(Globals.ourOperationResult, product);

				setResult(RESULT_OK, i);
				finish();
			}
		});
	}
}
