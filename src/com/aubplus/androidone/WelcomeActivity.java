// First, we start with the package name, equivalent to what namespaces are in C++.
// You can consider it as a folder, nothing more.
package com.aubplus.androidone;

// 'import' is the exact same thing as 'include' in C++. It means you want to use other
// classes, whether defined by the Android OS itself (for those who start with 'android.xxxx') or
// defined by us (for those who could have started with something like 'com.aubplus.androidtwo').
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * This is our first Activity, called WelcomeActivity. All Activities should extend (i.e. inherit)
 * from the super/base class Activity.
 * 
 * @author karim
 */
public class WelcomeActivity extends Activity {

	// Declare the Objects we're going to need in this WelcomeActivity.
	private TextView welcomeTextView; // Simple welcome message.
	private Button enterButton; // Starts a new Activity: EmailActivity.
	private TextView specialNumberTextView; // Displays a number.
	private Button calculatorButton; // Starts a new Activity: CalculatorActivity.

	/**
	 * ' OnCreate' is the first function that is called automatically when you first launch an
	 * Android application. You can map it to the 'main' function in C++ or Java.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// The super class Activity has already a defined 'onCreate' method.
		// To override it, we need to use the annotation @Override just before
		// implementing the function.

		// Then we need to call the base class's 'onCreate' method as follows:
		super.onCreate(savedInstanceState);

		// Set the XML layout associated with this Activity.
		setContentView(R.layout.activity_welcome);

		// In the XML layout specified above, every UI element has an associated 'id'. To associate
		// each Java Object declared on the top of this Class, we need to use the 'findViewById'
		// method, which takes as parameter 'R.id.xxxx' with 'xxxx' being the id of a specific
		// element.
		// Note that 'findViewById' returns a View, not a TextView or a Button, but this View can be
		// casted to a TextView or a Button (or many others), since they all extend (inherit) View.
		welcomeTextView = (TextView) findViewById(R.id.welcomeTextView);
		enterButton = (Button) findViewById(R.id.enterButton);
		specialNumberTextView = (TextView) findViewById(R.id.specialNumberTextView);
		calculatorButton = (Button) findViewById(R.id.calculatorButton);

		/**
		 * Common Mistake: Do not forget to save the XML file, every time you edit it. Saving the
		 * XML file builds it automatically, which assigns to it a reference in the gen.R file,
		 * which is the file that allows you to use these XML ids in the Java code. You can find it
		 * in the 'gen' folder in the Package Explorer.
		 **/

		// Now that we have assigned the Java Object 'welcomeTextView' to its corresponding XML
		// element 'welcomeTextView', we can use it as we would expect.

		// Let's change the text it displays.
		welcomeTextView.setText("Do you want to enter?");

		// Now, we want to associate an action to a Button. To do that, we need to set an
		// 'OnClickListener', which listens to clicks on the screen.
		enterButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Intents are messaging objects used to request an action from another app
				// component. This app component can be in the same app (AndroidOne) or in a
				// different one (like Gmail!).

				// Explicit Intent! We specify exactly which Activity to start. In this case:
				// EmailActivity
				Intent i = new Intent(WelcomeActivity.this, EmailActivity.class);

				// Now, we want to take the value displayed in the specialNumberTextView.
				// But this specialNumberTextView is a TextView, and TextViews display a
				// CharSequence, which is very similar to String, and can thus be converted to
				// String using 'toString'.
				String value = specialNumberTextView.getText().toString();

				// Then we need to parse this String and get the Integer in it. We can use a
				// built-in Java Class called Integer to do so.
				int intValue = Integer.parseInt(value);

				// Once we parsed the integer we want, we can add it to the Intent's Extras.
				// Extras are additional parameters that you can add to Intents to hold specific
				// data. Extras are like Hash Tables, they take pairs of (key, value).
				i.putExtra(Globals.specialNumberExtra, intValue);

				// Then we start the Activity by passing the Intent we declared above to the
				// 'startActivity' method.
				startActivity(i);
			}
		});

		// Do we need to explain what happens here?
		calculatorButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// Explicit Intent!
				Intent i = new Intent(WelcomeActivity.this, CalculatorActivity.class);

				// That's the only new thing in these few lines.
				// When starting an Activity, you can sometimes expect to return a result to the
				// Activity that initially started it. For example, when you start a Camera Activity
				// you can expect it to return the picture taken to the Activity that launched this
				// Camera Activity.
				// For this, we use startActivityForResult and pass an additional parameter: a
				// request code, so that if the Activity we started can return more than one kind of
				// result depending on which request it received, we specify which request we are
				// asking for here.
				startActivityForResult(i, Globals.myRequestCode);
			}
		});
	}

	/**
	 * And here is where we get the result of the Activity we started at some point, and after it
	 * finishes (i.e. is closed). This method is called automatically, and has three inputs:
	 * requestCode, resultCode and data.
	 * 
	 * @param requestCode
	 *            The integer request code originally supplied to startActivityForResult(), allowing
	 *            you to identify who this result came from.
	 * @param resultCode
	 *            The integer result code returned by the child activity through its setResult().
	 * @param data
	 *            An Intent, which can return result data to the caller (various data can be
	 *            attached to Intent "extras").
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Globals.myRequestCode && resultCode == RESULT_OK) {
			// Remember: data is an Intent, and Intents are messaging Objects. They usually contain
			// needed pieces of information. In our case, we passed an Integer Extra in
			// CalculatorActivity (with the key 'Globals.specialNumberForResult'), we can get it
			// here as follows:
			int value = data.getIntExtra(Globals.ourOperationResult, 0);

			// Then convert it to a String.
			String text = "" + value;

			// Then set it to the 'specialNumberTextView'.
			specialNumberTextView.setText(text);
		}
	}
}
