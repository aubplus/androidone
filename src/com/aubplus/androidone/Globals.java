package com.aubplus.androidone;

/**
 * Class containing global variables, that are public (accessible from anywhere), static (no need to
 * create an Object of the Class 'Globals' to call them) and final (constant).
 * 
 * @author karim
 */
public class Globals {

	// Key used to pass the special number from WelcomeActivity to EmailActivity.
	public static final String specialNumberExtra = "specialNumberExtra";

	// Key used to pass the special number from CalculatorActivity to WelcomeActivity.
	public static final String ourOperationResult = "specialNumberResult";

	// Request code (can be any number, really any number). The important thing is to make sure that
	// 'startActivityForResult' (which takes as a parameter a requestCode) and 'onActivityResult'
	// (which takes as a parameter a requestCode too) request and process the same request
	// respectively. To make sure it's the same request, we use the same requestCode.
	public static final int myRequestCode = 515;
}
