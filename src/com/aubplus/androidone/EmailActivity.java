package com.aubplus.androidone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * This is another Activity, called EmailActivity. In it, we will write an email, specify a
 * recipient and a subject, then use other apps to send this email.
 * 
 * @author karim
 */
public class EmailActivity extends Activity {

	private EditText subjectEditText;
	private EditText toEditText;
	private EditText messageEditText;
	private Button sendButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_email);

		// Gets the Intent that started this Activity.
		Intent i = getIntent();

		// Gets its extras. (Remember that Extras are like Hash Tables!)
		Bundle extras = i.getExtras();

		// Gets the value for key 'specialNumberExtra' that *you* defined in the Globals class.
		int value = extras.getInt(Globals.specialNumberExtra);

		subjectEditText = (EditText) findViewById(R.id.subjectEditText);
		toEditText = (EditText) findViewById(R.id.toEditText);
		messageEditText = (EditText) findViewById(R.id.messageEditText);

		messageEditText.setText("My special number is " + value);

		sendButton = (Button) findViewById(R.id.sendButton);
		sendButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// Implicit Intent! We specify the Action to execute, not a specific Activity. Any
				// app that can execute this Action can be started.
				Intent intent = new Intent(Intent.ACTION_SEND);

				// We put Extras like we did before, with one notable difference: we are using
				// standard keys instead of custom keys (EXTRA_EMAIl, EXTRA_SUBJECT, EXTRA_TEXT).
				// Why?
				intent.putExtra(Intent.EXTRA_EMAIL, new String[] { toEditText.getText().toString() });
				intent.putExtra(Intent.EXTRA_SUBJECT, subjectEditText.getText().toString());
				intent.putExtra(Intent.EXTRA_TEXT, messageEditText.getText().toString());

				// Try changing the type to "text/html" or "message/rfc822".
				intent.setType("text/plain");

				// What if we want to force our users to use the Gmail app and not any other Email
				// app?
				// intent.setClassName("com.google.android.gm",
				// "com.google.android.gm.ComposeActivityGmail");

				startActivity(intent);
			}
		});
	}
}
